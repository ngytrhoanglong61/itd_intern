# Các bước có thể sử dụng tính năng:

1. Tải file hoặc clone repo về máy.
2. Tạo 1 instance mới của class MyBigNumber bằng lệnh new MyBigNumber(), truyền vào hai tham số với kiểu dữ liệu là string, đây sẽ là hai số được cộng với nhau.
3. Gọi hàm (method) sum trên instance đã tạo.
4. Hàm sẽ trả về 1 string là kết quả cộng của hai tham số được truyền vào.
5. Kiểm tra kết quả ở file Logging.txt được tự động log để check các bước tính toán xem có nhầm lẫn gì không.
