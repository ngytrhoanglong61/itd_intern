import fs from 'fs'

export class MyBigNumber {

    sum(stn1, stn2) {
      let sign = 0;
      let result = ''
      let data = ''
      const diff = Math.abs(stn1.length - stn2.length)
      //Push the bigger number first
      if (stn2.length > stn1.length) {
        let temp = stn1;
        stn1 = stn2;
        stn2 = temp;
      }
      for (let i = stn1.length - 1; i >= 0; i--)
      {
          console.log(stn1, stn2)
          const prevSign = sign;
          let sum = 0;
          if(i - diff >= 0) {
            sum = Number(stn1[i]) + Number(stn2[i - diff]) + sign;
          } else {
            sum = Number(stn1[i]) + sign;
          }
          sign = Math.floor(sum / 10);
          result = (sum % 10).toString() + result;
          data = data + Number(stn1[i]) + ' + ' + Number(stn2[i]) + ' + ' + prevSign + '= '+ sum + '=> Nhớ: ' + sign + '|| Result: ' + result + '\n'
        }
        
        if (sign) {
            result = '1' + result;
        }

        fs.writeFile('Logging.txt', data, (err) => {
        if (err) throw err;
        })
      console.log(diff);
      return result;
    }
  }
